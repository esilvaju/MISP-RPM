%{!?pear_metadir: %global pear_metadir %{pear_phpdir}}
%{!?__pear: %{expand: %%global __pear %{_bindir}/pear}}
%global pear_name Crypt_GPG

Name:           php-pear-Crypt-GPG
Version:        1.4.0
Release:        1%{?dist}
Summary:        GNU Privacy Guard (GnuPG)

Group:          Development/Libraries
License:        LGPL
URL:            http://pear.php.net/package/Crypt_GPG
Source0:        http://pear.php.net/get/%{pear_name}-%{version}.tgz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  php-pear(PEAR)

Requires(post): %{__pear}
Requires(postun): %{__pear}
Requires:       php-pear(PEAR)
Requires:       php-pear(Console_CommandLine) >= 1.1.10
Requires:       php-pear(PEAR) >= 1.4.0
Provides:       php-pear(Crypt_GPG) = %{version}

%description
This package provides an object oriented interface to GNU Privacy Guard
(GnuPG). It requires the GnuPG executable to be on the system.

Though GnuPG can support symmetric-key cryptography, this package is
intended only to facilitate public-key cryptography.

This package requires PHP version 5.2.1 or greater.

%prep
%setup -q -c
[ -f package2.xml ] || mv package.xml package2.xml
mv package2.xml %{pear_name}-%{version}/%{name}.xml

cd %{pear_name}-%{version}


%build
cd %{pear_name}-%{version}
# Empty build section, most likely nothing required.


%install
cd %{pear_name}-%{version}
rm -rf $RPM_BUILD_ROOT
%{__pear} install --nodeps --packagingroot $RPM_BUILD_ROOT %{name}.xml

# Clean up unnecessary files
rm -rf $RPM_BUILD_ROOT%{pear_metadir}/.??*

# Install XML package description
mkdir -p $RPM_BUILD_ROOT%{pear_xmldir}
install -pm 644 %{name}.xml $RPM_BUILD_ROOT%{pear_xmldir}


%clean
rm -rf $RPM_BUILD_ROOT


%post
%{__pear} install --nodeps --soft --force --register-only \
    %{pear_xmldir}/%{name}.xml >/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    %{__pear} uninstall --nodeps --ignore-errors --register-only \
        pear.php.net/%{pear_name} >/dev/null || :
fi


%files
%defattr(-,root,root,-)
%doc %{pear_docdir}/%{pear_name}


%{pear_xmldir}/%{name}.xml
# Expand this as needed to avoid owning dirs owned by our dependencies
# and to avoid unowned dirs
%{pear_phpdir}/Crypt/GPG/ByteUtils.php
%{pear_phpdir}/Crypt/GPG/DecryptStatusHandler.php
%{pear_phpdir}/Crypt/GPG/Engine.php
%{pear_phpdir}/Crypt/GPG/Exceptions.php
%{pear_phpdir}/Crypt/GPG/Key.php
%{pear_phpdir}/Crypt/GPG/KeyGenerator.php
%{pear_phpdir}/Crypt/GPG/KeyGeneratorErrorHandler.php
%{pear_phpdir}/Crypt/GPG/KeyGeneratorStatusHandler.php
%{pear_phpdir}/Crypt/GPG/PinEntry.php
%{pear_phpdir}/Crypt/GPG/ProcessControl.php
%{pear_phpdir}/Crypt/GPG/Signature.php
%{pear_phpdir}/Crypt/GPG/SignatureCreationInfo.php
%{pear_phpdir}/Crypt/GPG/SubKey.php
%{pear_phpdir}/Crypt/GPG/UserId.php
%{pear_phpdir}/Crypt/GPG/VerifyStatusHandler.php
%{pear_phpdir}/Crypt/GPG.php
%{pear_phpdir}/Crypt/GPGAbstract.php
%{pear_datadir}/Crypt_GPG
%{pear_testdir}/Crypt_GPG
%{_bindir}/crypt-gpg-pinentry

%changelog
