%{!?pear_metadir: %global pear_metadir %{pear_phpdir}}
%{!?__pear: %{expand: %%global __pear %{_bindir}/pear}}
%global pear_name CakePHP
%global pear_channel pear.cakephp.org

Name:           php-pear-%{pear_name}
Version:        2.5.7
Release:        1%{?dist}
Summary:        %{pear_name} Rapid Development Framework

Group:          Development/Libraries
License:        MIT
URL:            http://%{pear_channel}/package/%{pear_name}
Source0:        http://%{pear_channel}/get/%{pear_name}-%{version}.tgz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  php-pear(PEAR)

Requires(post): %{__pear}
Requires(postun): %{__pear}
Requires:       php-pear(PEAR)
Provides:       php-pear(%{pear_channel}/%{pear_name}) = %{version}
BuildRequires:  php-channel-cakephp-1.3
Requires:       php-channel-cakephp-1.3

%description
%{pear_name} is an application development framework for PHP 5.2+

CakePHP makes building web applications simpler, faster and require less code.

%prep
%setup -q -c
[ -f package2.xml ] || mv package.xml package2.xml
mv package2.xml %{pear_name}-%{version}/%{name}.xml

cd %{pear_name}-%{version}


%build
cd %{pear_name}-%{version}
# Empty build section, most likely nothing required.


%install
cd %{pear_name}-%{version}
rm -rf $RPM_BUILD_ROOT
%{__pear} install --nodeps --packagingroot $RPM_BUILD_ROOT %{name}.xml

# Clean up unnecessary files
rm -rf $RPM_BUILD_ROOT%{pear_metadir}/.??*

# Install XML package description
mkdir -p $RPM_BUILD_ROOT%{pear_xmldir}
install -pm 644 %{name}.xml $RPM_BUILD_ROOT%{pear_xmldir}


%clean
rm -rf $RPM_BUILD_ROOT


%post
%{__pear} install --nodeps --soft --force --register-only \
    %{pear_xmldir}/%{name}.xml >/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    %{__pear} uninstall --nodeps --ignore-errors --register-only \
        %{pear_channel}/%{pear_name} >/dev/null || :
fi


%files
%defattr(-,root,root,-)



%{pear_xmldir}/%{name}.xml
# Expand this as needed to avoid owning dirs owned by our dependencies
# and to avoid unowned dirs
%{pear_phpdir}/Cake/basics.php
%{pear_phpdir}/Cake/bootstrap.php
%{pear_phpdir}/Cake/LICENSE.txt
%{pear_phpdir}/Cake/VERSION.txt
%{pear_phpdir}/Cake/Cache/Cache.php
%{pear_phpdir}/Cake/Cache/CacheEngine.php
%{pear_phpdir}/Cake/Cache/Engine/ApcEngine.php
%{pear_phpdir}/Cake/Cache/Engine/FileEngine.php
%{pear_phpdir}/Cake/Cache/Engine/MemcachedEngine.php
%{pear_phpdir}/Cake/Cache/Engine/MemcacheEngine.php
%{pear_phpdir}/Cake/Cache/Engine/RedisEngine.php
%{pear_phpdir}/Cake/Cache/Engine/WincacheEngine.php
%{pear_phpdir}/Cake/Cache/Engine/XcacheEngine.php
%{pear_phpdir}/Cake/Config/cacert.pem
%{pear_phpdir}/Cake/Config/config.php
%{pear_phpdir}/Cake/Config/routes.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/1e00_1eff.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/1f00_1fff.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/2c00_2c5f.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/2c60_2c7f.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/2c80_2cff.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0080_00ff.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0100_017f.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0180_024F.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0250_02af.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0370_03ff.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0400_04ff.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0500_052f.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/0530_058f.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/2100_214f.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/2150_218f.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/2460_24ff.php
%{pear_phpdir}/Cake/Config/unicode/casefolding/ff00_ffef.php
%{pear_phpdir}/Cake/Configure/ConfigReaderInterface.php
%{pear_phpdir}/Cake/Configure/IniReader.php
%{pear_phpdir}/Cake/Configure/PhpReader.php
%{pear_phpdir}/Cake/Console/ConsoleErrorHandler.php
%{pear_phpdir}/Cake/Console/ConsoleInput.php
%{pear_phpdir}/Cake/Console/ConsoleInputArgument.php
%{pear_phpdir}/Cake/Console/ConsoleInputOption.php
%{pear_phpdir}/Cake/Console/ConsoleInputSubcommand.php
%{pear_phpdir}/Cake/Console/ConsoleOptionParser.php
%{pear_phpdir}/Cake/Console/ConsoleOutput.php
%{pear_phpdir}/Cake/Console/HelpFormatter.php
%{pear_phpdir}/Cake/Console/Shell.php
%{pear_phpdir}/Cake/Console/ShellDispatcher.php
%{pear_phpdir}/Cake/Console/TaskCollection.php
%{pear_phpdir}/Cake/Console/Command/AclShell.php
%{pear_phpdir}/Cake/Console/Command/ApiShell.php
%{pear_phpdir}/Cake/Console/Command/AppShell.php
%{pear_phpdir}/Cake/Console/Command/BakeShell.php
%{pear_phpdir}/Cake/Console/Command/CommandListShell.php
%{pear_phpdir}/Cake/Console/Command/CompletionShell.php
%{pear_phpdir}/Cake/Console/Command/ConsoleShell.php
%{pear_phpdir}/Cake/Console/Command/I18nShell.php
%{pear_phpdir}/Cake/Console/Command/SchemaShell.php
%{pear_phpdir}/Cake/Console/Command/ServerShell.php
%{pear_phpdir}/Cake/Console/Command/TestShell.php
%{pear_phpdir}/Cake/Console/Command/TestsuiteShell.php
%{pear_phpdir}/Cake/Console/Command/UpgradeShell.php
%{pear_phpdir}/Cake/Console/Command/Task/BakeTask.php
%{pear_phpdir}/Cake/Console/Command/Task/CommandTask.php
%{pear_phpdir}/Cake/Console/Command/Task/ControllerTask.php
%{pear_phpdir}/Cake/Console/Command/Task/DbConfigTask.php
%{pear_phpdir}/Cake/Console/Command/Task/ExtractTask.php
%{pear_phpdir}/Cake/Console/Command/Task/FixtureTask.php
%{pear_phpdir}/Cake/Console/Command/Task/ModelTask.php
%{pear_phpdir}/Cake/Console/Command/Task/PluginTask.php
%{pear_phpdir}/Cake/Console/Command/Task/ProjectTask.php
%{pear_phpdir}/Cake/Console/Command/Task/TemplateTask.php
%{pear_phpdir}/Cake/Console/Command/Task/TestTask.php
%{pear_phpdir}/Cake/Console/Command/Task/ViewTask.php
%{pear_phpdir}/Cake/Console/Templates/default/actions/controller_actions.ctp
%{pear_phpdir}/Cake/Console/Templates/default/classes/controller.ctp
%{pear_phpdir}/Cake/Console/Templates/default/classes/fixture.ctp
%{pear_phpdir}/Cake/Console/Templates/default/classes/model.ctp
%{pear_phpdir}/Cake/Console/Templates/default/classes/test.ctp
%{pear_phpdir}/Cake/Console/Templates/default/views/form.ctp
%{pear_phpdir}/Cake/Console/Templates/default/views/index.ctp
%{pear_phpdir}/Cake/Console/Templates/default/views/view.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/.htaccess
%{pear_phpdir}/Cake/Console/Templates/skel/index.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/acl.ini.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/acl.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/bootstrap.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/core.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/database.php.default
%{pear_phpdir}/Cake/Console/Templates/skel/Config/email.php.default
%{pear_phpdir}/Cake/Console/Templates/skel/Config/routes.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/Schema/db_acl.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/Schema/db_acl.sql
%{pear_phpdir}/Cake/Console/Templates/skel/Config/Schema/i18n.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/Schema/i18n.sql
%{pear_phpdir}/Cake/Console/Templates/skel/Config/Schema/sessions.php
%{pear_phpdir}/Cake/Console/Templates/skel/Config/Schema/sessions.sql
%{pear_phpdir}/Cake/Console/Templates/skel/Console/cake
%{pear_phpdir}/Cake/Console/Templates/skel/Console/cake.bat
%{pear_phpdir}/Cake/Console/Templates/skel/Console/cake.php
%{pear_phpdir}/Cake/Console/Templates/skel/Console/Command/AppShell.php
%{pear_phpdir}/Cake/Console/Templates/skel/Console/Command/Task/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Console/Templates/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Controller/AppController.php
%{pear_phpdir}/Cake/Console/Templates/skel/Controller/PagesController.php
%{pear_phpdir}/Cake/Console/Templates/skel/Controller/Component/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Lib/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Locale/eng/LC_MESSAGES/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Model/AppModel.php
%{pear_phpdir}/Cake/Console/Templates/skel/Model/Behavior/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Model/Datasource/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Plugin/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Test/Case/AllTestsTest.php
%{pear_phpdir}/Cake/Console/Templates/skel/Test/Case/Controller/Component/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Test/Case/Model/Behavior/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Test/Case/View/Helper/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Test/Fixture/empty
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/cache/models/empty
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/cache/persistent/empty
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/cache/persistent/myapp_cake_core_cake_console_
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/cache/persistent/myapp_cake_core_cake_console_eng
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/cache/persistent/myapp_cake_core_cake_dev_eng
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/cache/views/empty
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/logs/empty
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/sessions/empty
%{pear_phpdir}/Cake/Console/Templates/skel/tmp/tests/empty
%{pear_phpdir}/Cake/Console/Templates/skel/Vendor/empty
%{pear_phpdir}/Cake/Console/Templates/skel/View/Elements/empty
%{pear_phpdir}/Cake/Console/Templates/skel/View/Emails/html/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Emails/text/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Errors/error400.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Errors/error500.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Helper/AppHelper.php
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/ajax.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/error.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/flash.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/Emails/html/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/Emails/text/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/js/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/rss/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Layouts/xml/default.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Pages/home.ctp
%{pear_phpdir}/Cake/Console/Templates/skel/View/Scaffolds/empty
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/.htaccess
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/favicon.ico
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/index.php
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/test.php
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/css/cake.generic.css
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/files/empty
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/img/cake.icon.png
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/img/cake.power.gif
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/img/test-error-icon.png
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/img/test-fail-icon.png
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/img/test-pass-icon.png
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/img/test-skip-icon.png
%{pear_phpdir}/Cake/Console/Templates/skel/webroot/js/empty
%{pear_phpdir}/Cake/Controller/CakeErrorController.php
%{pear_phpdir}/Cake/Controller/Component.php
%{pear_phpdir}/Cake/Controller/ComponentCollection.php
%{pear_phpdir}/Cake/Controller/Controller.php
%{pear_phpdir}/Cake/Controller/Scaffold.php
%{pear_phpdir}/Cake/Controller/Component/AclComponent.php
%{pear_phpdir}/Cake/Controller/Component/AuthComponent.php
%{pear_phpdir}/Cake/Controller/Component/CookieComponent.php
%{pear_phpdir}/Cake/Controller/Component/EmailComponent.php
%{pear_phpdir}/Cake/Controller/Component/PaginatorComponent.php
%{pear_phpdir}/Cake/Controller/Component/RequestHandlerComponent.php
%{pear_phpdir}/Cake/Controller/Component/SecurityComponent.php
%{pear_phpdir}/Cake/Controller/Component/SessionComponent.php
%{pear_phpdir}/Cake/Controller/Component/Acl/AclInterface.php
%{pear_phpdir}/Cake/Controller/Component/Acl/DbAcl.php
%{pear_phpdir}/Cake/Controller/Component/Acl/IniAcl.php
%{pear_phpdir}/Cake/Controller/Component/Acl/PhpAcl.php
%{pear_phpdir}/Cake/Controller/Component/Auth/AbstractPasswordHasher.php
%{pear_phpdir}/Cake/Controller/Component/Auth/ActionsAuthorize.php
%{pear_phpdir}/Cake/Controller/Component/Auth/BaseAuthenticate.php
%{pear_phpdir}/Cake/Controller/Component/Auth/BaseAuthorize.php
%{pear_phpdir}/Cake/Controller/Component/Auth/BasicAuthenticate.php
%{pear_phpdir}/Cake/Controller/Component/Auth/BlowfishAuthenticate.php
%{pear_phpdir}/Cake/Controller/Component/Auth/BlowfishPasswordHasher.php
%{pear_phpdir}/Cake/Controller/Component/Auth/ControllerAuthorize.php
%{pear_phpdir}/Cake/Controller/Component/Auth/CrudAuthorize.php
%{pear_phpdir}/Cake/Controller/Component/Auth/DigestAuthenticate.php
%{pear_phpdir}/Cake/Controller/Component/Auth/FormAuthenticate.php
%{pear_phpdir}/Cake/Controller/Component/Auth/SimplePasswordHasher.php
%{pear_phpdir}/Cake/Core/App.php
%{pear_phpdir}/Cake/Core/CakePlugin.php
%{pear_phpdir}/Cake/Core/Configure.php
%{pear_phpdir}/Cake/Core/Object.php
%{pear_phpdir}/Cake/Error/ErrorHandler.php
%{pear_phpdir}/Cake/Error/ExceptionRenderer.php
%{pear_phpdir}/Cake/Error/exceptions.php
%{pear_phpdir}/Cake/Event/CakeEvent.php
%{pear_phpdir}/Cake/Event/CakeEventListener.php
%{pear_phpdir}/Cake/Event/CakeEventManager.php
%{pear_phpdir}/Cake/I18n/I18n.php
%{pear_phpdir}/Cake/I18n/L10n.php
%{pear_phpdir}/Cake/I18n/Multibyte.php
%{pear_phpdir}/Cake/Log/CakeLog.php
%{pear_phpdir}/Cake/Log/CakeLogInterface.php
%{pear_phpdir}/Cake/Log/LogEngineCollection.php
%{pear_phpdir}/Cake/Log/Engine/BaseLog.php
%{pear_phpdir}/Cake/Log/Engine/ConsoleLog.php
%{pear_phpdir}/Cake/Log/Engine/FileLog.php
%{pear_phpdir}/Cake/Log/Engine/SyslogLog.php
%{pear_phpdir}/Cake/Model/AclNode.php
%{pear_phpdir}/Cake/Model/Aco.php
%{pear_phpdir}/Cake/Model/AcoAction.php
%{pear_phpdir}/Cake/Model/Aro.php
%{pear_phpdir}/Cake/Model/BehaviorCollection.php
%{pear_phpdir}/Cake/Model/CakeSchema.php
%{pear_phpdir}/Cake/Model/ConnectionManager.php
%{pear_phpdir}/Cake/Model/I18nModel.php
%{pear_phpdir}/Cake/Model/Model.php
%{pear_phpdir}/Cake/Model/ModelBehavior.php
%{pear_phpdir}/Cake/Model/ModelValidator.php
%{pear_phpdir}/Cake/Model/Permission.php
%{pear_phpdir}/Cake/Model/Behavior/AclBehavior.php
%{pear_phpdir}/Cake/Model/Behavior/ContainableBehavior.php
%{pear_phpdir}/Cake/Model/Behavior/TranslateBehavior.php
%{pear_phpdir}/Cake/Model/Behavior/TreeBehavior.php
%{pear_phpdir}/Cake/Model/Datasource/CakeSession.php
%{pear_phpdir}/Cake/Model/Datasource/DataSource.php
%{pear_phpdir}/Cake/Model/Datasource/DboSource.php
%{pear_phpdir}/Cake/Model/Datasource/Database/Mysql.php
%{pear_phpdir}/Cake/Model/Datasource/Database/Postgres.php
%{pear_phpdir}/Cake/Model/Datasource/Database/Sqlite.php
%{pear_phpdir}/Cake/Model/Datasource/Database/Sqlserver.php
%{pear_phpdir}/Cake/Model/Datasource/Session/CacheSession.php
%{pear_phpdir}/Cake/Model/Datasource/Session/CakeSessionHandlerInterface.php
%{pear_phpdir}/Cake/Model/Datasource/Session/DatabaseSession.php
%{pear_phpdir}/Cake/Model/Validator/CakeValidationRule.php
%{pear_phpdir}/Cake/Model/Validator/CakeValidationSet.php
%{pear_phpdir}/Cake/Network/CakeRequest.php
%{pear_phpdir}/Cake/Network/CakeResponse.php
%{pear_phpdir}/Cake/Network/CakeSocket.php
%{pear_phpdir}/Cake/Network/Email/AbstractTransport.php
%{pear_phpdir}/Cake/Network/Email/CakeEmail.php
%{pear_phpdir}/Cake/Network/Email/DebugTransport.php
%{pear_phpdir}/Cake/Network/Email/MailTransport.php
%{pear_phpdir}/Cake/Network/Email/SmtpTransport.php
%{pear_phpdir}/Cake/Network/Http/BasicAuthentication.php
%{pear_phpdir}/Cake/Network/Http/DigestAuthentication.php
%{pear_phpdir}/Cake/Network/Http/HttpResponse.php
%{pear_phpdir}/Cake/Network/Http/HttpSocket.php
%{pear_phpdir}/Cake/Network/Http/HttpSocketResponse.php
%{pear_phpdir}/Cake/Routing/Dispatcher.php
%{pear_phpdir}/Cake/Routing/DispatcherFilter.php
%{pear_phpdir}/Cake/Routing/Router.php
%{pear_phpdir}/Cake/Routing/Filter/AssetDispatcher.php
%{pear_phpdir}/Cake/Routing/Filter/CacheDispatcher.php
%{pear_phpdir}/Cake/Routing/Route/CakeRoute.php
%{pear_phpdir}/Cake/Routing/Route/PluginShortRoute.php
%{pear_phpdir}/Cake/Routing/Route/RedirectRoute.php
%{pear_phpdir}/Cake/Test/Fixture/AccountFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AcoActionFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AcoFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AcoTwoFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AdFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AdvertisementFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AfterTreeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AnotherArticleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AppleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArmorFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArmorsPlayerFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AroFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArosAcoFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArosAcoTwoFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AroTwoFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArticleFeaturedFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArticleFeaturedsTagsFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArticleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ArticlesTagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AssertTagsTestCase.php
%{pear_phpdir}/Cake/Test/Fixture/AttachmentFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AuthorFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AuthUserCustomFieldFixture.php
%{pear_phpdir}/Cake/Test/Fixture/AuthUserFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BakeArticleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BakeArticlesBakeTagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BakeCommentFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BakeTagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BasketFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BiddingFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BiddingMessageFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BidFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BinaryTestFixture.php
%{pear_phpdir}/Cake/Test/Fixture/BookFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CacheTestModelFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CakeSessionFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CallbackFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CampaignFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CategoryFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CategoryThreadFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CdFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CommentFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ContentAccountFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ContentFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CounterCachePostFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CounterCachePostNonstandardPrimaryKeyFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CounterCacheUserFixture.php
%{pear_phpdir}/Cake/Test/Fixture/CounterCacheUserNonstandardPrimaryKeyFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DataTestFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DatatypeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DependencyFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DeviceFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DeviceTypeCategoryFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DeviceTypeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DocumentDirectoryFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DocumentFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DomainFixture.php
%{pear_phpdir}/Cake/Test/Fixture/DomainsSiteFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ExteriorTypeCategoryFixture.php
%{pear_phpdir}/Cake/Test/Fixture/FeaturedFixture.php
%{pear_phpdir}/Cake/Test/Fixture/FeatureSetFixture.php
%{pear_phpdir}/Cake/Test/Fixture/FilmFileFixture.php
%{pear_phpdir}/Cake/Test/Fixture/FixturizedTestCase.php
%{pear_phpdir}/Cake/Test/Fixture/FlagTreeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/FruitFixture.php
%{pear_phpdir}/Cake/Test/Fixture/FruitsUuidTagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/GroupUpdateAllFixture.php
%{pear_phpdir}/Cake/Test/Fixture/GuildFixture.php
%{pear_phpdir}/Cake/Test/Fixture/GuildsPlayerFixture.php
%{pear_phpdir}/Cake/Test/Fixture/HomeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ImageFixture.php
%{pear_phpdir}/Cake/Test/Fixture/InnoFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ItemFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ItemsPortfolioFixture.php
%{pear_phpdir}/Cake/Test/Fixture/JoinABFixture.php
%{pear_phpdir}/Cake/Test/Fixture/JoinACFixture.php
%{pear_phpdir}/Cake/Test/Fixture/JoinAFixture.php
%{pear_phpdir}/Cake/Test/Fixture/JoinBFixture.php
%{pear_phpdir}/Cake/Test/Fixture/JoinCFixture.php
%{pear_phpdir}/Cake/Test/Fixture/JoinThingFixture.php
%{pear_phpdir}/Cake/Test/Fixture/MessageFixture.php
%{pear_phpdir}/Cake/Test/Fixture/MyCategoriesMyProductsFixture.php
%{pear_phpdir}/Cake/Test/Fixture/MyCategoriesMyUsersFixture.php
%{pear_phpdir}/Cake/Test/Fixture/MyCategoryFixture.php
%{pear_phpdir}/Cake/Test/Fixture/MyProductFixture.php
%{pear_phpdir}/Cake/Test/Fixture/MyUserFixture.php
%{pear_phpdir}/Cake/Test/Fixture/NodeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/NumberTreeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/NumberTreeTwoFixture.php
%{pear_phpdir}/Cake/Test/Fixture/NumericArticleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/OverallFavoriteFixture.php
%{pear_phpdir}/Cake/Test/Fixture/PersonFixture.php
%{pear_phpdir}/Cake/Test/Fixture/PlayerFixture.php
%{pear_phpdir}/Cake/Test/Fixture/PortfolioFixture.php
%{pear_phpdir}/Cake/Test/Fixture/PostFixture.php
%{pear_phpdir}/Cake/Test/Fixture/PostsTagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/PrefixTestFixture.php
%{pear_phpdir}/Cake/Test/Fixture/PrimaryModelFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ProductFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ProductUpdateAllFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ProjectFixture.php
%{pear_phpdir}/Cake/Test/Fixture/SampleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/SecondaryModelFixture.php
%{pear_phpdir}/Cake/Test/Fixture/SessionFixture.php
%{pear_phpdir}/Cake/Test/Fixture/SiteFixture.php
%{pear_phpdir}/Cake/Test/Fixture/SomethingElseFixture.php
%{pear_phpdir}/Cake/Test/Fixture/SomethingFixture.php
%{pear_phpdir}/Cake/Test/Fixture/StoriesTagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/StoryFixture.php
%{pear_phpdir}/Cake/Test/Fixture/SyfileFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TestPluginArticleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TestPluginCommentFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ThePaperMonkiesFixture.php
%{pear_phpdir}/Cake/Test/Fixture/ThreadFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TranslateArticleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TranslatedArticleFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TranslatedItemFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TranslateFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TranslateTableFixture.php
%{pear_phpdir}/Cake/Test/Fixture/TranslateWithPrefixFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UnconventionalTreeFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UnderscoreFieldFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UnsignedFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UserFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UuidFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UuiditemFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UuiditemsUuidportfolioFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UuiditemsUuidportfolioNumericidFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UuidportfolioFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UuidTagFixture.php
%{pear_phpdir}/Cake/Test/Fixture/UuidTreeFixture.php
%{pear_phpdir}/Cake/TestSuite/CakeTestCase.php
%{pear_phpdir}/Cake/TestSuite/CakeTestLoader.php
%{pear_phpdir}/Cake/TestSuite/CakeTestRunner.php
%{pear_phpdir}/Cake/TestSuite/CakeTestSuite.php
%{pear_phpdir}/Cake/TestSuite/CakeTestSuiteCommand.php
%{pear_phpdir}/Cake/TestSuite/CakeTestSuiteDispatcher.php
%{pear_phpdir}/Cake/TestSuite/ControllerTestCase.php
%{pear_phpdir}/Cake/TestSuite/Coverage/BaseCoverageReport.php
%{pear_phpdir}/Cake/TestSuite/Coverage/HtmlCoverageReport.php
%{pear_phpdir}/Cake/TestSuite/Coverage/TextCoverageReport.php
%{pear_phpdir}/Cake/TestSuite/Fixture/CakeFixtureManager.php
%{pear_phpdir}/Cake/TestSuite/Fixture/CakeTestFixture.php
%{pear_phpdir}/Cake/TestSuite/Fixture/CakeTestModel.php
%{pear_phpdir}/Cake/TestSuite/Reporter/CakeBaseReporter.php
%{pear_phpdir}/Cake/TestSuite/Reporter/CakeHtmlReporter.php
%{pear_phpdir}/Cake/TestSuite/Reporter/CakeTextReporter.php
%{pear_phpdir}/Cake/TestSuite/templates/footer.php
%{pear_phpdir}/Cake/TestSuite/templates/header.php
%{pear_phpdir}/Cake/TestSuite/templates/menu.php
%{pear_phpdir}/Cake/TestSuite/templates/missing_connection.php
%{pear_phpdir}/Cake/TestSuite/templates/phpunit.php
%{pear_phpdir}/Cake/TestSuite/templates/xdebug.php
%{pear_phpdir}/Cake/Utility/CakeNumber.php
%{pear_phpdir}/Cake/Utility/CakeTime.php
%{pear_phpdir}/Cake/Utility/ClassRegistry.php
%{pear_phpdir}/Cake/Utility/Debugger.php
%{pear_phpdir}/Cake/Utility/File.php
%{pear_phpdir}/Cake/Utility/Folder.php
%{pear_phpdir}/Cake/Utility/Hash.php
%{pear_phpdir}/Cake/Utility/Inflector.php
%{pear_phpdir}/Cake/Utility/ObjectCollection.php
%{pear_phpdir}/Cake/Utility/Sanitize.php
%{pear_phpdir}/Cake/Utility/Security.php
%{pear_phpdir}/Cake/Utility/Set.php
%{pear_phpdir}/Cake/Utility/String.php
%{pear_phpdir}/Cake/Utility/Validation.php
%{pear_phpdir}/Cake/Utility/Xml.php
%{pear_phpdir}/Cake/View/Helper.php
%{pear_phpdir}/Cake/View/HelperCollection.php
%{pear_phpdir}/Cake/View/JsonView.php
%{pear_phpdir}/Cake/View/MediaView.php
%{pear_phpdir}/Cake/View/ScaffoldView.php
%{pear_phpdir}/Cake/View/ThemeView.php
%{pear_phpdir}/Cake/View/View.php
%{pear_phpdir}/Cake/View/ViewBlock.php
%{pear_phpdir}/Cake/View/XmlView.php
%{pear_phpdir}/Cake/View/Elements/exception_stack_trace.ctp
%{pear_phpdir}/Cake/View/Elements/sql_dump.ctp
%{pear_phpdir}/Cake/View/Errors/fatal_error.ctp
%{pear_phpdir}/Cake/View/Errors/missing_action.ctp
%{pear_phpdir}/Cake/View/Errors/missing_behavior.ctp
%{pear_phpdir}/Cake/View/Errors/missing_component.ctp
%{pear_phpdir}/Cake/View/Errors/missing_connection.ctp
%{pear_phpdir}/Cake/View/Errors/missing_controller.ctp
%{pear_phpdir}/Cake/View/Errors/missing_database.ctp
%{pear_phpdir}/Cake/View/Errors/missing_datasource.ctp
%{pear_phpdir}/Cake/View/Errors/missing_datasource_config.ctp
%{pear_phpdir}/Cake/View/Errors/missing_helper.ctp
%{pear_phpdir}/Cake/View/Errors/missing_layout.ctp
%{pear_phpdir}/Cake/View/Errors/missing_plugin.ctp
%{pear_phpdir}/Cake/View/Errors/missing_table.ctp
%{pear_phpdir}/Cake/View/Errors/missing_view.ctp
%{pear_phpdir}/Cake/View/Errors/pdo_error.ctp
%{pear_phpdir}/Cake/View/Errors/private_action.ctp
%{pear_phpdir}/Cake/View/Errors/scaffold_error.ctp
%{pear_phpdir}/Cake/View/Helper/CacheHelper.php
%{pear_phpdir}/Cake/View/Helper/FormHelper.php
%{pear_phpdir}/Cake/View/Helper/HtmlHelper.php
%{pear_phpdir}/Cake/View/Helper/JqueryEngineHelper.php
%{pear_phpdir}/Cake/View/Helper/JsBaseEngineHelper.php
%{pear_phpdir}/Cake/View/Helper/JsHelper.php
%{pear_phpdir}/Cake/View/Helper/MootoolsEngineHelper.php
%{pear_phpdir}/Cake/View/Helper/NumberHelper.php
%{pear_phpdir}/Cake/View/Helper/PaginatorHelper.php
%{pear_phpdir}/Cake/View/Helper/PrototypeEngineHelper.php
%{pear_phpdir}/Cake/View/Helper/RssHelper.php
%{pear_phpdir}/Cake/View/Helper/SessionHelper.php
%{pear_phpdir}/Cake/View/Helper/TextHelper.php
%{pear_phpdir}/Cake/View/Helper/TimeHelper.php
%{pear_phpdir}/Cake/View/Scaffolds/form.ctp
%{pear_phpdir}/Cake/View/Scaffolds/index.ctp
%{pear_phpdir}/Cake/View/Scaffolds/view.ctp
%{pear_datadir}/%{pear_name}

%{_bindir}/cake
%{_bindir}/cake.bat
%{_bindir}/cake.php

%changelog
* Mon Dec 15 2014 Megh Parikh <meghprkh@gmail.com> 2.5.7-1
- Initial RPM release.
