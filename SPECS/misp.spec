%define _prefix /var/www/MISP

Name:	MISP	
Version:2.3
Release:1%{?dist}
Summary:MISP - Malware Information Sharing Platform

Group:System Environment/Application	
License:COPYRIGHT
URL:https://github.com/MISP/MISP
BuildArch:x86_64
Source: %{name}-%{version}.tgz

Prefix: %{_prefix}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

Requires:gcc git httpd zip php redis mysql-server php-mysql python-devel python-pip libxslt-devel zlib-devel php-devel php-xml php-mbstring php-pear php-pecl-geoip python-lxml python-dateutil python-six php-pecl-redis mariadb php-pear-Net-GeoIP php-pear-Crypt-GPG cybox stix php-pear-CakePHP php-channel-cakephp php-composer-installers

%description
MISP - Malware Information Sharing Platform

%prep
%setup -n %{name}

%install

#Create directory where the files will be located
mkdir -p $RPM_BUILD_ROOT/var/www/MISP

# Put the files in to the relevant directory
cp -r ../MISP  $RPM_BUILD_ROOT/var/www/

%post

# Configurantion of scheduler worker for scheduled tasks
cp -fa $RPM_BUILD_ROOT/%{_prefix}/INSTALL/setup/config.php $RPM_BUILD_ROOT/%{_prefix}/app/Plugin/CakeResque/Config/config.php


# There are 4 sample configuration files in /var/www/MISP/app/Config that need to be copied
#cd $RPM_BUILD_ROOT/var/www/MISP/app/Config
#cp -a bootstrap.default.php bootstrap.php
#cp -a database.default.php database.php
#cp -a core.default.php core.php
#cp -a config.default.php config.php

#Set permissions
find $RPM_BUILD_ROOT/var/www/MISP -type d -exec chmod g=rx {} \;
chmod -R g+r,o= /var/www/MISP
chown apache:apache $RPM_BUILD_ROOT%{_prefix}/app/files
chown apache:apache $RPM_BUILD_ROOT%{_prefix}/app/files/terms
chown apache:apache $RPM_BUILD_ROOT%{_prefix}/app/files/scripts/tmp
chown apache:apache $RPM_BUILD_ROOT%{_prefix}/app/Plugin/CakeResque/tmp
chown -R apache:apache $RPM_BUILD_ROOT%{_prefix}/app/tmp
chown -R apache:apache $RPM_BUILD_ROOT%{_prefix}/app/webroot/img/orgs
chown -R apache:apache $RPM_BUILD_ROOT%{_prefix}/app/webroot/img/custom


# Open a hole in the iptables firewall
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --reload

%clean

rm -rf $RPM_BUILD_ROOT
rm -rf %{_tmppath}/%{name}

%files
%{_prefix}*


%changelog

* Tue Nov 24 2015  Eraldo Silva Junior
- 1.0 r1 First release
