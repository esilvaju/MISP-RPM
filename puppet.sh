#/bin/sh

# MISP Post instalation Script

# Database server listen on localhost only
echo [mysqld] > /etc/my.cnf.d/bind-address.cnf
echo bind-address=127.0.0.1 >> /etc/my.cnf.d/bind-address.cnf
systemctl restart mariadb.service

# Create misp database and set default password
mysql -u root -p </var/www/MISP/INSTALL/misp.sql

cd /var/www/MISP

# Import the empty MySQL database from MYSQL.sql
mysql -u misp -p misp < INSTALL/MYSQL.sql

# Now configure your apache server with the DocumentRoot /var/www/MISP/app/webroot/
# A sample ghost can be found in /var/www/MISP/INSTALL/apache.misp

cp /var/www/MISP/INSTALL/apache.misp /etc/httpd/conf.d/misp.conf

# Enable and start redis
systemctl enable redis.service
systemctl start  redis.service

# Enable, start and secure your mysql database server
systemctl enable mariadb.service
systemctl start  mariadb.service
mysql_secure_installation

# Allow httpd to connect to the redis server over tcp/ip
setsebool -P httpd_can_network_connect on

# Enable and start the httpd service
systemctl enable httpd.service
systemctl start  httpd.service

# Configure the fields in the newly created files:
# config.php   : baseurl
# database.php : login, port, password, database

# To enable the background workers, if you have installed the package required for it in 4/, uncomment the following lines:
# in core.php (if you have just recently updated MISP, just add this line at the end of the file):
# require_once dirname(__DIR__) . '/Vendor/autoload.php';

# Important! Change the salt key in /var/www/MISP/app/Config/config.php
# The admin user account will be generated on the first login, make sure that the salt is changed before you create that user
# If you forget to do this step, and you are still dealing with a fresh installation, just alter the salt,
# delete the user from mysql and log in again using the default admin credentials (admin@admin.test / admin)

# If you want to be able to change configuration parameters from the webinterface:
chown apache:apache /var/www/MISP/app/Config/config.php
chcon -t httpd_sys_content_rw_t /var/www/MISP/app/Config/config.php

# Generate a GPG encryption key.
mkdir /var/www/MISP/.gnupg
chmod 700 /var/www/MISP/.gnupg

# If the following command gives an error message, try it as root from the console
# can't connect to `/var/www/MISP/.gnupg/S.gpg-agent': No such file or directory
gpg --homedir /var/www/MISP/.gnupg --gen-key
chown -R apache:apache /var/www/MISP/.gnupg

# The email address should match the one set in the config.php configuration file
# Make sure that you use the same settings in the MISP Server Settings tool (Described on line 226)

# And export the public key to the webroot
sudo -u apache gpg --homedir /var/www/MISP/.gnupg --export --armor YOUR-EMAIL > /var/www/MISP/app/webroot/gpg.asc

# Start the workers to enable background jobs
su -s /bin/bash apache -c 'bash /var/www/MISP/app/Console/worker/start.sh'

# To make the background workers start on boot
echo su -s /bin/bash apache -c 'bash /var/www/MISP/app/Console/worker/start.sh' >> /etc/rc.local

# Now log in using the webinterface:
# Now log in using the webinterface:
# The default user/pass = admin@admin.test/admin

# Using the server settings tool in the admin interface (Administration -> Server Settings), set MISP up to your preference
# It is especially vital that no critical issues remain!

#Don't forget to change the email, password and authentication key after installation.

# Once done, have a look at the diagnostics

# If any of the directories that MISP uses to store files is not writeable to the apache user, change the permissions
# you can do this by running the following commands:

#chmod -R 750 /var/www/MISP/<directory path with an indicated issue>
#chown -R apache:apache /var/www/MISP/<directory path with an indicated issue>

# Make sure that the STIX libraries and GnuPG work as intended, if not, refer to INSTALL.txt's paragraphs dealing with these two items

# If anything goes wrong, make sure that you check MISP's logs for errors:
# /var/www/MISP/app/tmp/logs/error.log
# /var/www/MISP/app/tmp/logs/resque-worker-error.log
# /var/www/MISP/app/tmp/logs/resque-scheduler-error.log
# /var/www/MISP/app/tmp/logs/resque-2015-01-01.log //where the actual date is the current date

#Recommended actions
#-------------------
#- By default CakePHP exposes his name and version in email headers. Apply a patch to remove this behavior.

#- You should really harden your OS
#- You should really harden the configuration of Apache
#- You should really harden the configuration of MySQL
#- Keep your software up2date (MISP, CakePHP and everything else)
#- Log and audit

